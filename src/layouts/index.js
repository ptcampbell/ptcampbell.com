import React from 'react'
import PropTypes from 'prop-types'
import Helmet from 'react-helmet'
import { injectGlobal } from 'styled-components'

import '../fonts/openarrow/stylesheet.css'
import '../fonts/circular/stylesheet.css'

const TemplateWrapper = ({ children }) => (
	<div>
		<Helmet
			title="Patrick Campbell"
			meta={[
				{ name: 'description', content: 'Product design and React development' },
				{ name: 'keywords', content: 'Interface design and front-end web development based in Seattle, WA' },
			]}
		/>
		{children()}
	</div>
)

TemplateWrapper.propTypes = {
	children: PropTypes.func,
}

export default TemplateWrapper

injectGlobal`
	body {
		width:100%;
		height:100%;
		background-color: #326C35;
		color: #fff;
		margin:0;
		padding:0;
		font-family: 'IBM Plex Mono', monospace;
		font-size:14px;
		line-height:24px;
		font-weight:400;
	}
`
