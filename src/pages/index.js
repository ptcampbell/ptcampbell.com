import React from 'react'
import styled from 'styled-components'
import bg from "../images/diamonds.svg"
import img from "../images/pitch.jpg"

const size = {
	mobileS: '320px',
	mobileM: '375px',
	mobileL: '425px',
	tablet: '768px',
	laptop: '1024px',
	laptopL: '1440px',
	desktop: '2560px'
}

export const device = {
	tablet: `(min-width: ${size.tablet})`,
	desktop: `(min-width: ${size.desktop})`,
}

export default class IndexPage extends React.Component {

	state = {
		isExpanded: false,
	}

	handleExpand = () => {
		this.setState({
			isExpanded: true
		})
	}

	render() {
		const { isExpanded } = this.state

		return(
			<Main>
				<Content>
					<Image src={img} />
					<Text>
						<p>Patrick Campbell specialises in designing and prototyping apps, websites.</p>
						<p>Based in Seattle. Building React UI for Microsoft.</p>
						{!isExpanded ?
							<p><More onClick={this.handleExpand}>+ more</More></p>
							:
							<div>
								<p>
									Currently focused on design systems, React, Laravel, Typescript and Shopify apps.
								</p>

								<p>
									Formerly of <a href="http://studiopda.com.au">PDA</a>.
								</p>

								<p>
									Elsewhere on <a href="https://instagram.com/ptcampbell">Instagram</a>, <a href="https://soundcloud.com/patrice-life">Soundcloud</a>, <a href="#">Twitter</a>, <a href="https://github.com/ptcampbell">Github</a>, <a href="https://www.are.na/patrick-campbell">Are.na</a>, <a href="https://www.linkedin.com/in/ptcampbell/">LinkedIn</a>.
								</p>

								<p>
									<span>pat [at] ptcampbell.me</span>
								</p>
							</div>
						}

					</Text>
				</Content>
				<Background bg={bg} />
			</Main>
		)
	}
}



const Main = styled.main`
	display:flex;
	height:100vh;
	align-items:center;
	justify-content: center;
	background-color: #2F2E33;

	@media ${device.tablet} {
		background:none;
	}
`

const More = styled.a`
	cursor: pointer;
	text-decoration:none;
`

const Content = styled.div`
	flex: none;
	position: relative;
	margin:0px;

	@media ${device.tablet} {
		margin-left:-150px;
	}
`

const Text = styled.div`
	position: relative;
	width: 250px;
	z-index: 2;
	padding: 0;

	span {
		opacity:0.5;
	}

	a {
		color:#fff;
	}

	p {
		margin:0 0 20px;

		&:last-of-type{
			margin:0;
		}
	}

	@media ${device.tablet} {
		width: 290px;
		padding: 20px 0 0 210px;
	}
`

const Image = styled.img`
	width: 300px;
	height: 300px;
	position: absolute;
	top: calc(50% - 150px);
	left: 0;
	z-index: 0;
	display:none;

	@media ${device.tablet} {
		display:block;
	}
`

const Background = styled.div`
	position: fixed;
	width: 50%;
	max-width: 600px;
	height: 100%;
	right: 0;
	top: 0;
	background: none;
	background-size: 100%;
	background-position:0 50%;
	
	@media ${device.tablet} {
		background-image: url(${bg});
	}
`
