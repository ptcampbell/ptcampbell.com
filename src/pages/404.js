import React from 'react'

const NotFoundPage = () => (
  <div>
    <h1>404</h1>
    <p>Error beep boop</p>
  </div>
)

export default NotFoundPage
