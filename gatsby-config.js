module.exports = {
  siteMetadata: {
    title: `Patrick Campbell &mdash; Design and development`,
  },
  plugins: [
    {
      resolve: `gatsby-plugin-react-helmet`
    },
    {
      resolve: `gatsby-plugin-styled-components`
    },
    {
      resolve: `gatsby-plugin-google-fonts`,
      options: {
        fonts: [
          `ibm plex sans\:400,500`,
          'IBM Plex Mono'
        ]
      }
    },
    {
      resolve: `gatsby-plugin-google-analytics`,
      options: {
        trackingId: `UA-15105683-2`
      }
    }
  ]
}

